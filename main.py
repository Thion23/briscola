import game #importa il file del gioco
import messaggi #importa i file con i messaggi da visualizzare a schermo

[pozzo,briscola]=game.inizializza() #Mischia le carte e gira la prima carta "briscola", dopodiche' va alla fine del mazzo.
messaggi.puliscischermo() #Pulisce lo schermo e stampa il titolo.
print "\nInserisci i nomi dei due giocatori."
game.gioco(pozzo, briscola) #Fa partire il gioco.
