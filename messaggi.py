import os #Importa i comandi di sistema. Viene usata la system call per pulire lo schermo. E anche per dire che ho studiato Sistemi Operativi.
import carte #Importa le classi delle carte.

def pressanykey(): #Serve per mettere in pausa l'esecuzione e far decidere all'utente quando proseguire.
    raw_input("\nPremi Invio per continuare.\n") #Attende una stringa da parte dell'utente, ma questa non viene salvata.

def puliscischermo(): #Cancella tutto lo schermo
    try: #Linux
        os.system("clear") #System Call
    except: #Windows
        os.system("clc")
    #Non gestisce il Mac, perche' questo programma va contro la Apple.
    print "       *** BRISCOLA ***" #Stampa il titolo

def turno(giocatore): #Dice di chi e' il turno
    print "\n\nE' il turno di " + giocatore.Nome
    pressanykey()

def nonvalido(): #Autoesplicativo
    print "Scelta non valida."

def giocata(briscola,piatto): #Stampa a schermo la situazione di gioco
    #puliscischermo() #Cancella lo schermo
    if piatto!=carte.carta(): #Se nel piatto c'e' una carta...
        print "Sul piatto c'e':\n"+str(piatto) #... stampa la carta.
    print "\nBriscola: " +str(briscola)+"\nLe tue carte: " #Stampa la briscola.

def dominata(piatto,giocatori,vincitore): #Stampa chi vince il turno
    puliscischermo() #Pulisce lo schermo per non far vedere le carte.
    print "\n\n"+str(piatto[0]) #Stampa la prima carta
    if vincitore==0: #capisce chi vince e stampa di conseguenza.
        print " batte "
    else:
        print " e' battuta da "
    print str(piatto[1])+"\nLe carte vanno a "+giocatori[vincitore].Nome #Dice a chi vanno le carte.

