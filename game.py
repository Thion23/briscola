import carte #Importa le classi delle carte
import messaggi #Importa i messaggi
import string #Importa la libreria delle stringhe: viene usata per le scritte in maiuscolo della vittoria.

def inizializza(): #Inizializza il gioco
    pozzo=carte.mazzo() #Pozzo e' diventato un mazzo di 40 carte a semi napoletani. Le carte sono in ordine
    pozzo.mischia() #Le carte vengono mischiate (vedi il metodo della classe "Mazzo")
    briscola=pozzo.Carte[0] #L'ultima carta e' la briscola. Non viene tolta dal mazzo.
    return [pozzo,briscola] #Restituisce una lista contente il mazzo mischiato e la briscola.

def sceltaturno(g1,g2,briscola,pozzo,flag=0): #A seconda del valore di flag, si decide di chi e' il turno
    if len(g1.Carte)==0: #Se sono state giocate tutte le carte...
	return #... finisci il gioco
    if flag == 0: #Se il flag e' 0, e' il turno del primo giocatore
        carta1 = turno(g1, briscola, pozzo) #Il giocatore gioca e scende una carta
        messaggi.puliscischermo() #Si pulisce lo schermo per non far vedere all'altro giocatore le carte
        carta2 = turno(g2, briscola, pozzo,carta1) #Il secondo giocatore gioca vedendo la carta che ha buttato l'avversario
    else: #Altrimenti e' il turno dell'altro giocatore
        carta2 = turno(g2, briscola, pozzo) #I turni sono invertiti
        messaggi.puliscischermo()
        carta1 = turno(g1, briscola, pozzo,carta2)
    flag=dominare([carta1, carta2], [g1, g2], briscola,flag) #Restituisce il turno (assegna anche i punteggi, vedi la funzione)
    messaggi.dominata([carta1,carta2],[g1,g2],flag) #Stampa a schermo cosa e' successo
    sceltaturno(g1,g2,briscola,pozzo,flag) #Ricorsione con il turno cambiato

def gioco(pozzo,briscola): #Si gioca!
    g1=carte.Mano(raw_input("Nome giocatore 1: ")) #Chiede all'utente il nome del giocatore e inizializza...
    g2=carte.Mano(raw_input("Nome giocatore 2: ")) #... due oggetti della classe mano
    pozzo.distribuisci([g1,g2],4) #Vengono distribuite 4 carte, quindi due ciascuno: la terza viene data nel turno.
    sceltaturno(g1,g2,briscola,pozzo) #Si gioca con il turno al primo giocatore
	#Il gioco e' finito e stampa i messaggi di vincita
    g1.stampapunti() #Stampa le carte conquistate dai giocatori
    g2.stampapunti()
    print g1.Nome+" ha totalizzato "+str(g1.conteggio)+" punti."  #Stampa i punteggi
    print g2.Nome + " ha totalizzato " + str(g2.conteggio) + " punti."
    if g1.conteggio>g2.conteggio: #Dice chi ha vinto
        print "\n\n"+string.upper(g1.Nome)+", HAI VINTO!\n"
    elif g1.conteggio<g2.conteggio:
        print "\n\n" + string.upper(g2.Nome) + ", HAI VINTO!\n"
    else:
        print "E' pari."

def turno(giocatore,briscola,pozzo,avversario=carte.carta()): #Se non ha carta in ingresso, prende "Indefinito"
    messaggi.turno(giocatore) #Dice a chi tocca (Vedi la funzione)
    try: #Prova a...
        giocatore.AggiungiCarta(pozzo.Carte.pop()) #... prendere una carta dal mazzo e darla al giocatore di turno.
        print "Carte rimanenti: "+str(len(pozzo.Carte))
    except: #Se non ci riesce, e' finito il mazzo...
        pass #... e quindi non fa nulla.
    messaggi.giocata(briscola,avversario) #Stampa la briscola e l'eventuale carta buttata dall'avversario (vedi la funzione)
    print giocatore #Stampa le carte in mano
    while 1:
        try:
            numerocarta=input("Scegli la carta: ") #Fa scegliere la carta
            carta=giocatore.Carte[numerocarta-1] #Prende la carta scelta...
            giocatore.rimuovi(carta) #... la rimuove dalla mano...
            return carta #... e la restituisce alla funzione "Scelta turno" per il confronto
        except: #Se ci sono errori, ha scelto una carta non valida...
            messaggi.nonvalido() #... e lo fa notare senza troppi complimenti.

def dominare(piatto,giocatore,briscola,flag): #Questa funzione decide chi vince
    if piatto[0].seme==piatto[1].seme: #Se i semi sono uguali si verificano i punteggi (Vedi il metodo __cmp__ delle Carte)
        if piatto[0]>piatto[1]: #Domina il giocatore 1
            giocatore[0].AggiungiPunti(piatto) #Aggiunge i punti al giocatore (Vedi la funzione)
            return 0 #Restituisce il turno
        else: #Domina il giocatore 2, stessa cosa
            giocatore[1].AggiungiPunti(piatto)
            return 1
    #Se invece il seme non e' lo stesso, il giocatore non di turno (reso come flag+1%2) prende le carte solo se ha buttato una briscola
    elif piatto[(flag+1)%2].seme==briscola.seme: #Quindi se e' una briscola...
        giocatore[(flag+1)%2].AggiungiPunti(piatto) #... si prende le carte...
        return (flag+1)%2 #... e si prende il turno.
    else: #Altrimenti...
        giocatore[flag].AggiungiPunti(piatto) #... vince l'altro giocatore...
        return flag #... e si prende il turno.
