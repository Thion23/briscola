import random #Importa la libreria per le funzioni che generano numeri casuali.

class carta:
    semi=["Denari","Bastoni","Coppe","Spade"] #Lista dei semi
    valori=["Indefinito","Asso","2","3","4","5","6","7","Fante","Cavallo","Re"] #Lista dei numeri...
    punteggio=[0,11,0,0,0,0,0,10,2,3,4] #... e punteggio di ogni carta

    def __init__(self,seme=0,valore=0): #Inizializza l'oggetto.
        self.seme=seme
        self.valore=valore
        self.punti=self.punteggio[valore]

    def __str__(self): #Come viene stampato: il numero, il seme e, tra parentesi, il punteggio
        return self.valori[self.valore] + " di " + self.semi[self.seme] + " ["+str(self.punti)+" punti]"

    def __cmp__(self,Altro): #Compara due carte
        if self.punti > Altro.punti: #Il punteggio ha la priorita'
            return 1
        elif self.punti < Altro.punti:
            return -1
        else:
            if self.valore > Altro.valore: #Segue il numero
                return 1
            elif self.valore < Altro.valore:
                return -1
            else:
                return 0 #Se hanno stesso numero, stanno nello stesso livello.

class mazzo:
    def __init__(self): #Inizializza un mazzo di carte.
        self.Carte=[] #Crea una lista vuota
        for seme in range(4): #Cicla i semi
            for valore in range(1,11): #Di ogni seme, cicla il numero
                self.Carte.append(carta(seme,valore)) #Ogni carta viene aggiunta alla lista

    def __str__(self): #Stampa: ogni riga e' un numero seguito dalla carta
        s = ""
        for i in range(len(self.Carte)):
            s = s + "[" +str(i+1)+"] " + str(self.Carte[i]) + "\n"
        return s

    def mischia(self): #Mischia le carte
        numerodicarte=len(self.Carte)
        for i in range(numerodicarte): #Cicla le carte
            j=random.randrange(i,numerodicarte) #Prende una generica carta dal mazzo...
            self.Carte[i],self.Carte[j]=self.Carte[j],self.Carte[i] #... e la scambia con la carta del ciclo

    def rimuovi(self,carta): #Toglie la carta data in ingresso dal mazzo
        if carta in self.Carte:
            self.Carte.remove(carta)
            return 1
        else:
            return 0

    def distribuisci(self,giocatori,numcarte=999): #Distribuisce le carte ai giocatori. "Giocatori" e' una lista di oggetti della classe "Mano"
        numerogiocatori=len(giocatori)
        for i in range(numcarte): #Scorre le carte
            if len(self.Carte)==0: #Se il mazzo e' finito...
                break #chiude il ciclo
            Carta=self.Carte.pop() #Prende la prima carta e la toglie dal mazzo
            Mano=giocatori[i%numerogiocatori] #Prende il giocatore di turno...
            Mano.AggiungiCarta(Carta) #... e gli da' la carta

class Mano(mazzo):
    def __init__(self,Nome="Giocatore"): #Inizializza un giocatore
        self.Carte=[] #Non ha carte
        self.Nome=Nome #Gli viene dato il nome in ingresso
        self.punti=[] #Non ha punti
        self.conteggio=0

    def AggiungiCarta(self,Carta): #Aggiunge la carta alla mano
        self.Carte.append(Carta)

    def __str__(self): #Stampa le carte che ha in mano
        return mazzo.__str__(self)

    def AggiungiPunti(self,Carte): #Aggiunge i punteggi. Prende in ingresso una lista di due carte.
        self.conteggio = self.conteggio + Carte[0].punti + Carte[1].punti #Aggiunge i punti
        #print self.conteggio
        self.punti.append(Carte[0]) #Aggiunge le carte alla lista dei punti
        self.punti.append(Carte[1])

    def stampapunti(self): #Stampa le carte dei punti; la partita deve essere necessariamente finita
        self.Carte=self.punti #Trasferisce le carte dei punti nel mazzo...
        print "I punti di "+self.Nome+"\n"
        print mazzo.__str__(self) #... e stampa il mazzo
